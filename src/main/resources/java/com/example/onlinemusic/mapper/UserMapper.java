package com.example.onlinemusic.mapper;

import com.example.onlinemusic.model.User;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface UserMapper {

    /**
     * 根据user的用户名和密码查询User对象
     * @param user
     * @return
     */
    User login(User user);

    /**
     * 通过username 查询User
     * @param username
     * @return
     */
    User selectByName(String username);
}
