package com.example.onlinemusic.mapper;

import com.example.onlinemusic.model.Music;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

@Mapper
public interface LoveMusicMapper {
    /**
     * 根据userid 和 musicid 查询是否有喜欢的音乐
     * @param userId
     * @param musicId
     * @return
     */
    Music findLoveMusicByUserIdAndMusicId(int userId,int musicId);

    /**
     * 根据userID和musicid 添加到喜欢的音乐
     * @param userId
     * @param musicId
     * @return
     */
    int insertLoveMusic(int userId,int musicId);

    /**
     * 查询用户喜欢的所有音乐
     * @param userId
     * @return
     */
    List<Music> findLoveMusicAndUserId(int userId);

    /**
     * 查询用户喜欢的指定的音乐
     * @param musicName
     * @param userId
     * @return
     */
    List<Music> findLoveMusicByUserIdAndMusicName(String musicName,int userId);

    /**
     * 删除该用户喜欢的音乐 从音乐列表删除 而不是从数据库中删
     * @param userId
     * @param musicId
     * @return
     */
    int deleteLoveMusic(int userId,int musicId);

    /**
     * 当数据库中删除音乐,同步删除lovemusic中的数据
     * @param musicId
     * @return
     */
    int deleteLoveMusicById(int musicId);
}
