package com.example.onlinemusic.mapper;

import com.example.onlinemusic.model.Music;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

@Mapper
public interface MusicMapper {
    /**
     * 插入音乐
     * @param title
     * @param singer
     * @param url
     * @param time
     * @param
     * @return
     */
    int insert(String title, String singer, String url, String time, int userId);

    /**
     * 根据 title 和 singer 查询音乐
     * @param title
     * @param singer
     * @return
     */
    Music selectByMusic(String title, String singer);

    /**
     * 更据ID查询音乐
     * @param id
     * @return
     */
    Music findByMusic(int id);

    /**
     * 根据ID删除音乐
     * @param musicId
     * @return
     */
    int deleteMusicById(int musicId);

    /**
     * 查询所有music
     * @return
     */
    List<Music> findAllMusic();

    /**
     * 根据指定名称 查询音乐
     * @param title
     * @return
     */
    List<Music> findMusicByName(String title);
}
