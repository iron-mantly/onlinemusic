package com.example.onlinemusic.controller;

import com.example.onlinemusic.mapper.LoveMusicMapper;
import com.example.onlinemusic.mapper.MusicMapper;
import com.example.onlinemusic.model.Music;
import com.example.onlinemusic.model.User;
import com.example.onlinemusic.tools.Comtant;
import com.example.onlinemusic.tools.ResponseBodyMessage;
import org.apache.ibatis.binding.BindingException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.ResourceBundle;

@RestController
@RequestMapping("/music")
public class MusicController {

    @Value("${music.local.path}")//获取配置文件的信息
    String SEV_PATH;

    @Autowired
    private MusicMapper musicMapper;

    @Autowired
    private LoveMusicMapper loveMusicMapper;

    /**
     * 上传音乐
     * @param singer
     * @param file
     * @param request
     * @return
     */
    @RequestMapping("/upload")
    public ResponseBodyMessage<Boolean> insertMusic(@RequestParam String singer, @RequestParam("filename")MultipartFile file, HttpServletRequest request){

        //1.1检查是否登录
        HttpSession session = request.getSession(false);
        if (session==null||session.getAttribute(Comtant.USERINFO_SESSION_KEY)==null){
            return new ResponseBodyMessage<>(-1,"上传失败,请登录",false);
        }
        //获取上传文件名称
        String filename = file.getOriginalFilename();

        //获取title
        int point =filename.lastIndexOf(".");
        String title = filename.substring(0,point);

        //1.2先查询数据库中是否有音乐
        Music music=musicMapper.selectByMusic(title,singer);

        //说明数据库中已经存在这条数据
        if (music!=null&&music.getTitle().equals(title)&&music.getSinger().equals(singer)){
            return new ResponseBodyMessage<>(-1,"数据库中已经存在这首歌,请重新上传",false);
        }

        //2.上传到服务器
        System.out.println("filename:"+filename);
        String path = SEV_PATH +"/"+ filename;

        File dest = new File(path);

        if (!dest.exists()){
            dest.mkdir();
        }
        try {
            //上传到服务器
            file.transferTo(dest);
            //return new ResponseBodyMessage<>(1,"上传成功",true);
        } catch (IOException e) {
            e.printStackTrace();
            return new ResponseBodyMessage<>(-1,"服务器上传失败",false);
        }

        //3.上传到数据库
        User user = (User)session.getAttribute(Comtant.USERINFO_SESSION_KEY);
        int userId = user.getId();

        String url = "/music/get?path="+title;

        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd");
        String time = simpleDateFormat.format(new Date());

        try{
            int ret =0;
            ret = musicMapper.insert(title,singer,url,time,userId);
            if (ret==1){
                return new ResponseBodyMessage<>(0,"数据库上传成功",true);
            }else{
                return new ResponseBodyMessage<>(-1,"数据库上传失败",false);
            }

        }catch (BindingException e){
            dest.delete();
            return new ResponseBodyMessage<>(-1,"数据库上传失败",false);
        }
    }

    /**
     * 返回音乐的字节码  .MP3格式的字节码都有 TAG
     * @param path
     * @return
     */
    @RequestMapping("/get")
    public ResponseEntity<byte[]> get(String path){
        File file = new File(SEV_PATH+"/"+path);
        byte[] bytes=null;
        try {
             bytes = Files.readAllBytes(file.toPath());
             if (bytes==null){
                 return ResponseEntity.badRequest().build();
             }
             //返回 xxx.mp3的字节码到客户端
             return ResponseEntity.ok(bytes);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return ResponseEntity.badRequest().build();
    }

    /**
     * 根据id删除 服务器和数据库中的音乐
     * @param id
     * @return
     */
    @RequestMapping("/delete")
    public ResponseBodyMessage<Boolean> deleteMusic(@RequestParam String id){
        int iid = Integer.parseInt(id);
        //先查询音乐是否存在
        Music music = musicMapper.findByMusic(iid);
        if (music==null){
            return new ResponseBodyMessage<>(-1,"没有你要删除的音乐",false);
        }else{
            //2.1 删除单个音乐
            //先删除数据库中的音乐
            int ret = musicMapper.deleteMusicById(iid);
            if (ret==1){
                //在删除服务器中的音乐
                String title = music.getTitle();
                File file = new File(SEV_PATH+"\\"+title+".mp3");
                System.out.println("此时的路径"+file.getPath());
                if (file.delete()){
                    loveMusicMapper.deleteLoveMusicById(iid);
                    return new ResponseBodyMessage<>(0,"删除音乐成功",true);

                }else{
                    return new ResponseBodyMessage<>(-1,"服务器删除音乐失败",false);
                }
            }else{
                return new ResponseBodyMessage<>(-1,"数据库删除音乐失败",false);
            }
        }
    }

    /**
     * 根据id批量删除音乐
     * @param id
     * @return
     */
    @RequestMapping("/deleteSel")
    public ResponseBodyMessage<Boolean> deleteSelMusic(@RequestParam("id[]")List<Integer> id){

        System.out.println("size:"+ Arrays.toString(id.toArray()));
        int sum =0;
        for (int i = 0; i < id.size(); i++) {
            //先查询音乐是否存在
            Music music = musicMapper.findByMusic(id.get(i));
            if (music==null){
                return new ResponseBodyMessage<>(-1,"没有你要删除的音乐",false);
            }
            //2.1 删除单个音乐
            //先删除数据库中的音乐
            int ret = musicMapper.deleteMusicById(id.get(i));
            if (ret==1) {
                //在删除服务器中的音乐
                String title = music.getTitle();
                File file = new File(SEV_PATH +"\\"+ title + ".mp3");
                System.out.println("此时的路径" + file.getPath());
                if (file.delete()) {
                    sum++;
                    loveMusicMapper.deleteLoveMusicById(id.get(i));
                } else {
                    return new ResponseBodyMessage<>(-1, "服务器删除音乐失败", false);
                }
            }else {
                return new ResponseBodyMessage<>(-1,"数据库删除音乐失败",false);
            }
        }
        if (sum==id.size()){
            return new ResponseBodyMessage<>(0,"批量删除音乐成功",true);
        }else {
            return new ResponseBodyMessage<>(-1,"批量删除音乐失败",false);
        }
    }

    /**
     * 根据参数查询音乐 参数可以为null
     * @param musicName
     * @return
     */
    @RequestMapping("/findmusic")
    public ResponseBodyMessage<List<Music>> findMusic(@RequestParam(required = false) String musicName){


        List<Music>  list = null;
        if (musicName==null){
            list= musicMapper.findAllMusic();
        }else{
            list = musicMapper.findMusicByName(musicName);
        }
        return new ResponseBodyMessage<>(0,"查询音乐成功",list);
    }

}
