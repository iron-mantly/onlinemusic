package com.example.onlinemusic.controller;

import com.example.onlinemusic.mapper.LoveMusicMapper;
import com.example.onlinemusic.model.Music;
import com.example.onlinemusic.model.User;
import com.example.onlinemusic.tools.Comtant;
import com.example.onlinemusic.tools.ResponseBodyMessage;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.awt.*;
import java.util.List;

@RestController
@RequestMapping("/lovemusic")
public class LoveMusicController {

    @Autowired
    private LoveMusicMapper loveMusicMapper;

    /**
     * 查找喜欢的音乐 如果没有就插入到喜欢的音乐中
     * @param id
     * @param request
     * @return
     */
    @RequestMapping("/likeMusic")
    public ResponseBodyMessage<Boolean> likeMusic(@RequestParam String id, HttpServletRequest request){

        int musicId = Integer.parseInt(id);
        System.out.println("likeMusic->musicId: "+musicId);
        HttpSession session = request.getSession(false);
        if (session==null||session.getAttribute(Comtant.USERINFO_SESSION_KEY)==null){
            System.out.println("没有登录");
            return new ResponseBodyMessage<>(-1,"没有登陆",false);
        }
        User user = (User) session.getAttribute(Comtant.USERINFO_SESSION_KEY);
        int userId = user.getId();
        System.out.println("likeMusic->userId: "+userId);

        Music music = loveMusicMapper.findLoveMusicByUserIdAndMusicId(userId, musicId);

        if (music!=null){
            return new ResponseBodyMessage<>(-1,"该用户已经点赞收藏",false);
        }else{
            //插入
            int ret = loveMusicMapper.insertLoveMusic(userId, musicId);
            if (ret==1){
                return new ResponseBodyMessage<>(0,"点赞收藏成功",true);
            }else{
                return new ResponseBodyMessage<>(-1,"点赞收藏失败",false);
            }
        }

    }

    /**
     * 插找用户喜欢的音乐 参数可以无参 也可模糊匹配
     * @param musicName
     * @param request
     * @return
     */
    @RequestMapping("/findlovemusic")
    public ResponseBodyMessage<List<Music>> findLoveMusic(@RequestParam(required = false) String musicName, HttpServletRequest request){
        List<Music> list = null;
        HttpSession session = request.getSession(false);
        if (session==null||session.getAttribute(Comtant.USERINFO_SESSION_KEY)==null){
            System.out.println("没有登录");
            return new ResponseBodyMessage<>(-1,"没有登陆",null);
        }
        User user = (User) session.getAttribute(Comtant.USERINFO_SESSION_KEY);
        int userId = user.getId();

        if (musicName==null){
             list = loveMusicMapper.findLoveMusicAndUserId(userId);
        }else{
            list= loveMusicMapper.findLoveMusicByUserIdAndMusicName(musicName,userId);
        }
        return new ResponseBodyMessage<>(0,"查询到了所有喜欢的音乐",list);
    }

    /**
     * 从喜欢列表中取消收藏/喜欢
     * @param mId
     * @param request
     * @return
     */
    @RequestMapping("/deletelovemusic")
    public ResponseBodyMessage<Boolean> deleteLoveMusic(@RequestParam String mId,HttpServletRequest request){
        int musicId = Integer.parseInt(mId);

        HttpSession session = request.getSession(false);
        if (session==null||session.getAttribute(Comtant.USERINFO_SESSION_KEY)==null){
            System.out.println("没有登录");
            return new ResponseBodyMessage<>(-1,"没有登陆,请先登录",false);
        }
        User user = (User) session.getAttribute(Comtant.USERINFO_SESSION_KEY);
        int userId = user.getId();

        int ret = loveMusicMapper.deleteLoveMusic(userId, musicId);
        if (ret==1){
            return new ResponseBodyMessage<>(0,"取消收藏成功",true);
        }else{
            return new ResponseBodyMessage<>(-1,"取消收藏失败",false);
        }
    }

}
